class App {
  constructor() {
    this.clearButton = document.getElementById("clear-btn");
    this.loadButton = document.getElementById("search-button");
    this.carContainerElement = document.getElementById("car-container-element");
    this.availDate = document.getElementById('avail-date')
    this.carCapacity = document.getElementById('car-capacity').value;
    
    
  }
  
  async init() {
    await this.load();

    // Register click listener
    this.clearButton.onclick = this.clear;
    this.loadButton.onclick = this.run;
  }

  run = () => {
    Car.list.forEach((car) => { 
      const node = document.createElement('div');
      node.className = 'col-lg-4'
      node.innerHTML = car.render();
      this.carContainerElement.append(node);
    });
  };

  async filtererLoad(carCapacity){
    console.log('car capacity '+carCapacity)
    const cars = await Binar.listCars((item) =>{
      return item.capacity > Number(carCapacity)
    })
    Car.init(cars)
  }

  async load() {
    const cars = await Binar.listCars();
    Car.init(cars);
  }

  clear = () => {
    let child = this.carContainerElement.firstElementChild;

    while (child) {
      child.remove();
      child = this.carContainerElement.firstElementChild;
    }
  };
}
